# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## [1.3.0] - 2019-07-27
### Added
- upgrade to node 12
- fix environments variable not being passed in the last stage

## [1.2.0] - 2019-05-25
### Added
- add favicon
- fix documentation
- Improve docker-compose
- Improve gitlab-ci
- Change application port to 80 instead 3000
- update packages
- healthcheck route
- integration tests

## [1.1.0] - 2019-04-14
### Added
 - Improve Dockerfile and docker-compose.yml for local development

## [1.0.0] - 2018-07-12
### Added
 - init boilerplate
