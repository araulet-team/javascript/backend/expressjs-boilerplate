/** @module routes/api/v1/ */
'use strict'

const _root = require('../../controllers/root')

const rootRte = [
  {
    url: '',
    method: 'get',
    handler: _root.get,
    description: 'routes discovery',
    notes: 'routes discovery',
    tags: ['api', 'discovery']
  }
]

module.exports = rootRte
