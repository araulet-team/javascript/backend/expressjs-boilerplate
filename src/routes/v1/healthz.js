/** @module routes/api/v1/ */
'use strict'

const _healthz = require('../../controllers/healthz')

const healthzRte = [
  {
    url: '/healthz/',
    method: 'get',
    handler: _healthz.get,
    description: 'healthcheck endpoint',
    notes: 'healthcheck endpoint',
    tags: ['api', 'healthz']
  }
]

module.exports = healthzRte
