/** @module controllers/healthz */
'use strict'

/**
 * healthz api
 * @param  {Object} req
 * @param  {Object} res
 * @return {Object}
 */
function get (req, res) {
  res.status(200).json({})
}

module.exports = {
  get
}
